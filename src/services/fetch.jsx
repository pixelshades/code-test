import superagent from "superagent";

const fetch = (url, type, data) => superagent[type](`${url}`).send(data);

export default fetch;
