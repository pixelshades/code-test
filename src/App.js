//React
import React, { Component } from "react";

//React Components
import Main from "routes/main";

export class App extends Component {
  componentDidMount() {}

  render() {
    return <Main />;
  }
}

export default App;
