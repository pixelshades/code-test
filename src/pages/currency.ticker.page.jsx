// React
import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

// Redux
import { connect } from "react-redux";
import { getCurrencyDiff } from "redux/modules/forex/compare";

// components
import CurrencyTicker from "components/currency.ticker";

// Material UI
import RaisedButton from "material-ui/RaisedButton";

// Styles using styled-components
const CurrencyTickerStyle = styled.div`
  margin: 0 auto;
  text-align: center;
`;

// You can get this from an end point but just hardcodding for now
const currencyConfig = [
  { from: "USD", to: "CHF" },
  { from: "EUR", to: "USD" },
  { from: "USD", to: "JPY" },
  { from: "DZD", to: "USD" },
  { from: "AUD", to: "AZN" },
  { from: "GBP", to: "INR" },
  { from: "NOK", to: "BRL" },
  { from: "BND", to: "BGN" },
  { from: "CAD", to: "KYD" },
  { from: "CLF", to: "CLP" }
];

const mapStateToProps = state => {
  return {
    currencyResponse: {
      isFetching: state.compareCurrencyState.isFetching,
      data: state.compareCurrencyState.data,
      error: state.compareCurrencyState.error
    }
  };
};

const mapDispatchToProps = {
  getCurrencyDiff
};

class CurrencyTickerPage extends Component {
  constructor(props) {
    super(props);
    this.getCurrenyUpdate = this.getCurrenyUpdate.bind(this);
  }
  static propTypes = {
    currencyResponse: PropTypes.shape({
      isFetching: PropTypes.bool,
      data: PropTypes.object,
      error: PropTypes.string
    })
  };
  state = {
    currencyCompareArray: []
  };
  componentDidMount() {
    this.getCurrenyUpdate();
  }
  getCurrenyUpdate() {
    let dataList = [];
    currencyConfig.forEach(currency => {
      dataList.push(this.props.getCurrencyDiff(currency.from, currency.to));
    });
    this.setState({ currencyCompareArray: dataList });
  }
  render() {
    const ROOT = this;
    let renderCurrencyTickerBox = [];
    const getCurrencyTicker = () => {
      ROOT.state.currencyCompareArray.forEach((currencyObj, index) => {
        if (currencyObj.length !== undefined) {
          renderCurrencyTickerBox.push(
            <CurrencyTicker data={currencyObj[0]} key={index} />
          );
        }
      });
    };
    getCurrencyTicker();
    return (
      <div>
        <CurrencyTickerStyle>
          <h1>Currency Ticker</h1>
          <RaisedButton
            label="Refresh exchange rate"
            primary={true}
            onClick={this.getCurrenyUpdate}
          />
          {renderCurrencyTickerBox}
        </CurrencyTickerStyle>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyTickerPage);
