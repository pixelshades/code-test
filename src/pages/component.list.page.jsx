// React
import React, { Component } from "react";
// import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";

// Redux
// import { connect } from "react-redux";

// Material ui
import Paper from "material-ui/Paper";
import Menu from "material-ui/Menu";
import MenuItem from "material-ui/MenuItem";

// Styles using styled-components
const ComponentListStyle = styled.div`background: white;`;

class ComponentList extends Component {
  render() {
    return (
      <ComponentListStyle>
        <h1>Code Test: Vijay Murukesan</h1>
        <a href="https://www.linkedin.com/in/vijaii/">Linkedin profile</a>
        <h2>List of components</h2>
        <Paper>
          <Menu>
            <Link to={`/home`} className="link">
              <MenuItem primaryText="Home" />
            </Link>
            <Link to={`/currency-ticker`} className="link">
              <MenuItem primaryText="Currency ticker" />
            </Link>
          </Menu>
        </Paper>
      </ComponentListStyle>
    );
  }
}

export default ComponentList;
