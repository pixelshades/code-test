//React
import React from "react";

//react components
import ComponentList from "pages/component.list.page";

//define states
const states = {};

const store = createMockStore(states);

it("Component List page should render without throwing an error", () => {
  const component = shallow(<ComponentList />, { context: { store } });
  expect(toJson(component)).toMatchSnapshot();
});
