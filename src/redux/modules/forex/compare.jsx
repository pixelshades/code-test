//Services
import api from "api";
import request from "services/request";

export const FETCH_REQUEST = "modules/forex/FETCH_REQUEST";
export const FETCH_SUCCESS = "modules/forex/FETCH_SUCCESS";
export const FETCH_ERROR = "modules/forex/FETCH_ERROR";

const options = [FETCH_REQUEST, FETCH_SUCCESS, FETCH_ERROR];

const initialState = {
  isFetching: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_REQUEST:
      return {
        ...state,
        isFetching: true
      };

    case FETCH_ERROR:
      return {
        ...state,
        error: action.error,
        isFetching: false
      };

    case FETCH_SUCCESS:
      return {
        ...state,
        data: action.data,
        isFetching: false
      };

    default:
      return state;
  }
};

export const getCurrencyDiff = (currencyFrom, currencyTo) => {
  let req = api["forex.compare.get"](currencyFrom, currencyTo);

  return (dispatch, getState) => {
    let data = [];
    request(req, options, dispatch, getState).then(res => {
      data.push(res.data);
    });
    return data;
  };
};
