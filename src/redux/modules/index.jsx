import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";

import compareCurrencyState from "redux/modules/forex/compare";

export default combineReducers({
  router: routerReducer,
  compareCurrencyState
});
