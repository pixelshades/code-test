import React from "react";
// import raf from "tempPolyfills";
import Enzyme, { shallow, render, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import toJson from "enzyme-to-json";
import configureStore from "redux-mock-store";

const createMockStore = configureStore();

// React 16 Enzyme adapter
Enzyme.configure({ adapter: new Adapter() });

// Make Enzyme functions available in all test files without importing
global.shallow = shallow;
global.render = render;
global.mount = mount;
global.toJson = toJson;
global.createMockStore = createMockStore;

// Create setup wrapper
global.setupWrapper = function(Component, defaultProps) {
  return function(overrideProps) {
    const props = {
      ...defaultProps,
      ...overrideProps
    };

    const wrapper = shallow(<Component {...props} />);

    return { wrapper };
  };
};

// Fail tests on any warning
console.error = message => {
  throw new Error(message);
};
