//React
import React from "react";

//react components
import CurrencyTicker from "components/currency.ticker";

//define states
const states = {};

const store = createMockStore(states);

it("currency.ticker component should render without throwing an error", () => {
  const props = {
    data: {
      "Realtime Currency Exchange Rate": {
        "1. From_Currency Code": "USD",
        "2. From_Currency Name": "United States Dollar",
        "3. To_Currency Code": "CHF",
        "4. To_Currency Name": "Swiss Franc",
        "5. Exchange Rate": "0.99825900",
        "6. Last Refreshed": "2018-05-04 10:32:03",
        "7. Time Zone": "UTC"
      }
    }
  };
  const component = shallow(<CurrencyTicker {...props} />, {
    context: { store }
  });
  expect(toJson(component)).toMatchSnapshot();
});
