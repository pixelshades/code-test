// React
import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

// Material ui
import Paper from "material-ui/Paper";

const PaperStyled = styled(Paper)`
  width: 200px;
  height: auto;
  display: inline-block;
  margin: 20px;
  text-align: center;
`;

class CurrencyTicker extends Component {
  static propTypes = {
    data: PropTypes.object
  };

  render() {
    let currencyData =
      typeof this.props.data !== "undefined" &&
      this.props.data["Realtime Currency Exchange Rate"];
    let systemError =
      typeof this.props.data !== "undefined" && this.props.data["Information"]
        ? true
        : false;
    const formateCurrency = currency => {
      // could use any formatting package like numeral to format the currency
      return currency;
    };
    return (
      <div>
        {typeof this.props.data !== "undefined" && systemError === false ? (
          <PaperStyled zDepth={1}>
            <h3>
              {"(1) " + currencyData["1. From_Currency Code"]} To {" "}
              {currencyData["3. To_Currency Code"]}
            </h3>
            <p>
              {formateCurrency(currencyData["5. Exchange Rate"])}
              {" - "}
              {currencyData["3. To_Currency Code"]}
            </p>
            <p>{currencyData["6. Last Refreshed"]}</p>
          </PaperStyled>
        ) : (
          <PaperStyled zDepth={1}>
            {systemError ? "Error Loading..." : "Loading..."}
          </PaperStyled>
        )}
      </div>
    );
  }
}

export default CurrencyTicker;
