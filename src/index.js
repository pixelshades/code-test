//React
import React from "react";
import { render } from "react-dom";

// Redux
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import store, { history } from "redux/store/store";

//Components
import App from "App";

// 3rd party components
// import { injectGlobal } from "styled-components";

// CSS
import "index.css";

const target = document.querySelector("#root");

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  target
);
