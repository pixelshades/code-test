import React from "react";
import Main from "routes/main";

it("Main component should render without throwing an error", () => {
  const component = shallow(<Main />);
  expect(toJson(component)).toMatchSnapshot();
});
