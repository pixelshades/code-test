//React
import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";

// Material ui
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

// pages
import ComponentList from "pages/component.list.page";
import CurrenyTicker from "pages/currency.ticker.page";

class Main extends Component {
  render() {
    return (
      <HashRouter>
        <MuiThemeProvider>
          <Switch>
            <Route exact path="/" component={ComponentList} />
            <Route exact path="/home" component={ComponentList} />
            <Route exact path="/:componentName" component={CurrenyTicker} />
          </Switch>
        </MuiThemeProvider>
      </HashRouter>
    );
  }
}

export default Main;
