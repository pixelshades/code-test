const api = {
  "forex.compare.get": (currencyFrom, currencyTo) => {
    return {
      url: `https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=${currencyFrom}&to_currency=${currencyTo}&apikey=0O7XMC4XV0R2GOHD`,
      type: "get",
      successful: "Project updated successfully.",
      error: "There was an error loading the current user information.",
      data: null
    };
  }
};

export default api;
