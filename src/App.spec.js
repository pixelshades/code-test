//React
import React from "react";

//react components
import App from "App";

//define states
const states = {};

const store = createMockStore(states);

it("App page should render without throwing an error", function() {
  const component = shallow(<App />, { context: { store } });
  expect(toJson(component)).toMatchSnapshot();
});
